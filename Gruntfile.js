// Generated on 2014-10-02 using
// generator-webapp 0.5.0
'use strict';

// # Globbing
// for performance reasons we're only matching one level down:
// 'test/spec/{,*/}*.js'
// If you want to recursively match all subfolders, use:
// 'test/spec/**/*.js'

module.exports = function (grunt) {

  // Time how long tasks take. Can help when optimizing build times
  require('time-grunt')(grunt);

  // Load grunt tasks automatically
  require('load-grunt-tasks')(grunt);

  // Configurable paths
  var config = {
    app: 'app',
    dist: 'dist'
  };

  // Define the configuration for all the tasks
  grunt.initConfig({

    // Project settings
    config: config,

    // Watches files for changes and runs tasks based on the changed files
    watch: {
      bower: {
        files: ['bower.json'],
        tasks: ['wiredep']
      },
      jstest: {
        files: ['test/spec/{,*/}*.js'],
        tasks: ['test:watch']
      },
      sass: {
        files: ['<%= config.app %>/styles/{,*/}*.{scss,sass}'],
        tasks: ['sass:server', 'imageEmbed', 'autoprefixer', 'parseCss']
      },
      styles: {
        files: ['<%= config.app %>/styles/{,*/}*.css'],
        tasks: ['newer:copy:styles', 'imageEmbed', 'autoprefixer', 'parseCss']
      },
      livereload: {
        options: {
          livereload: '<%= connect.options.livereload %>'
        },
        files: [
          '.tmp/{,*/}*.html',
          '.tmp/styles/{,*/}*.css',
          '<%= config.app %>/images/{,*/}*'
        ]
      }
    },

    // The actual grunt server settings
    connect: {
      options: {
        port: 9000,
        open: false,
        livereload: 35730,
        // Change this to '0.0.0.0' to access the server from outside
        hostname: 'localhost'
      },
      livereload: {
        options: {
          middleware: function(connect) {
            return [
              connect.static('.tmp'),
              connect.static(config.app+'/files'),
              connect().use('/bower_components', connect.static('./bower_components')),
              connect.static(config.app)
            ];
          }
        }
      },
      test: {
        options: {
          open: false,
          port: 9001,
          middleware: function(connect) {
            return [
              connect.static('.tmp'),
              connect.static('test'),
              connect().use('/bower_components', connect.static('./bower_components')),
              connect.static(config.app)
            ];
          }
        }
      },
      dist: {
        options: {
          base: '<%= config.dist %>',
          livereload: false
        }
      }
    },

    // Empties folders to start fresh
    clean: {
      dist: {
        files: [{
          dot: true,
          src: [
            '.tmp',
            '<%= config.dist %>/*',
            '!<%= config.dist %>/.git*'
          ]
        }]
      },
      server: '.tmp'
    },

    // Mocha testing framework configuration options
    mocha: {
      all: {
        options: {
          run: true,
          urls: ['http://<%= connect.test.options.hostname %>:<%= connect.test.options.port %>/index.html']
        }
      }
    },

    // Compiles Sass to CSS and generates necessary files if requested
    sass: {
      options: {
        loadPath: 'bower_components'
      },
      dist: {
        files: [{
          expand: true,
          cwd: '<%= config.app %>/styles',
          src: ['*.{scss,sass}'],
          dest: '.tmp/styles',
          ext: '.css'
        }]
      },
      server: {
        files: [{
          expand: true,
          cwd: '<%= config.app %>/styles',
          src: ['*.{scss,sass}'],
          dest: '.tmp/styles',
          ext: '.css'
        }]
      }
    },

    // Add vendor prefixed styles
    autoprefixer: {
      options: {
        browsers: ['last 2 Chrome versions']
      },
      dist: {
        files: [{
          expand: true,
          cwd: '.tmp/styles/',
          src: '{,*/}*.css',
          dest: '.tmp/styles/'
        }]
      }
    },

    // Automatically inject Bower components into the HTML file
    wiredep: {
      app: {
        ignorePath: /^\/|\.\.\//,
        src: ['./tmp/index.html']
      },
      sass: {
        src: ['<%= config.app %>/styles/{,*/}*.{scss,sass}'],
        ignorePath: /(\.\.\/){1,2}bower_components\//
      }
    },

    // Renames files for browser caching purposes
    rev: {
      dist: {
        files: {
          src: [
            '<%= config.dist %>/scripts/{,*/}*.js',
            '<%= config.dist %>/styles/{,*/}*.css',
            '<%= config.dist %>/images/{,*/}*.*',
            '<%= config.dist %>/styles/fonts/{,*/}*.*',
            '<%= config.dist %>/*.{ico,png}'
          ]
        }
      }
    },

    handlebars: {
      dist: {
        files: {
          '<%= config.app %>/scripts/templates.js': [
            '<%= config.app %>/templates/*.hbs'
          ],
        }
      }
    },

    // Copies remaining files to places other tasks can use
    copy: {
      themes: {
        expand: true,
        dot: true,
        cwd: '<%= config.app %>/scripts',
        dest: '<%= config.dist %>',
        src: 'themes.js'
      },
      styles: {
        expand: true,
        dot: true,
        cwd: '<%= config.app %>/styles',
        dest: '.tmp/styles/',
        src: '{,*/}*.css'
      },
      images: {
        expand: true,
        dot: true,
        cwd: '<%= config.app %>/images',
        dest: '.tmp/images/',
        src: '{,*/}*.{svg,png}'
      }
    },

    // Run some tasks in parallel to speed up build process
    concurrent: {
      server: [
        'sass:server',
        'copy:styles',
        'copy:images'
      ],
      test: [
        'copy:styles'
      ],
      dist: [
        'sass',
        'copy:styles',
        'copy:images'
      ]
    },

    json: {
      dist: {
        options: {
          namespace: 'p',
          processName: function () {
            return 'themes';
          }
        },
        src: '.tmp/scripts/themes.json',
        dest: '<%= config.app %>/scripts/themes.js'
      }
    },

    imageEmbed: {
      dist: {
        src: '.tmp/styles/main.css',
        dest: '.tmp/styles/main.css'
      }
    }

  });


  grunt.registerTask('serve', 'start the server and preview your app, --allow-remote for remote access', function (target) {
    if (grunt.option('allow-remote')) {
      grunt.config.set('connect.options.hostname', '0.0.0.0');
    }
    if (target === 'dist') {
      return grunt.task.run(['build', 'connect:dist:keepalive']);
    }

    grunt.task.run([
      'clean:server',
      'handlebars',
      'wiredep',
      'concurrent:server',
      'imageEmbed',
      'autoprefixer',
      'parseCss',
      'connect:livereload',
      'watch'
    ]);
  });

  grunt.registerTask('server', function (target) {
    grunt.log.warn('The `server` task has been deprecated. Use `grunt serve` to start a server.');
    grunt.task.run([target ? ('serve:' + target) : 'serve']);
  });

  grunt.registerTask('test', function (target) {
    if (target !== 'watch') {
      grunt.task.run([
        'clean:server',
        'handlebars',
        'concurrent:test',
        'imageEmbed',
        'autoprefixer',
        'parseCss'
      ]);
    }

    grunt.task.run([
      'connect:test',
      'mocha'
    ]);
  });

  grunt.registerTask('build', [
    'clean:dist',
    'concurrent:dist',
    'handlebars',
    'imageEmbed',
    'autoprefixer',
    'parseCss',
    'copy:themes'
  ]);

  grunt.registerTask('default', [
    'newer:jshint',
    'test',
    'build'
  ]);

  grunt.registerTask('parseCss', function () {
    var cssStr = grunt.file.read('.tmp/styles/main.css');
    var css = require('css');
    var obj = css.parse(cssStr);
    var selectorNames = [];

    var expandObj = function (obj, selectors, declarations) {
      var selector = selectors.shift();
      if (!obj[selector]) obj[selector] = {};
      if (selectors.length > 0) {
        expandObj(obj[selector], selectors, declarations);
      } else {
        obj[selector] = declarations;
        grunt.log.writeln('Setting '+selector+' selector');
        if (selectorNames.indexOf(selector) > -1) {
          grunt.fail.warn('Warning: You have a duplicate selector that may override other inline css calls.');
        }
        selectorNames.push(selector);
      }
    };

    var parseSelectors = function (str) {
      var items = str.split(' ');
      for (var i in items) {
        if (i < 2 && items[i].substring(0,1) === '.') {
          items[i] = items[i].substring(1);
        }
      }
      if (items.length === 2) {
        items.push('base');
      } else if (items.length > 2) {
        items.splice(2, 0, 'others');
      }
      return items;
    };

    var cssObj = {};
    var firstSelector;
    obj.stylesheet.rules.forEach(function (v) {
      var declarations = {};
      v.declarations.forEach(function (d) {
        declarations[d.property] = d.value;
      });
      v.selectors.forEach(function (s) {
        var selectors = parseSelectors(s);
        if (firstSelector !== selectors[0]) {
          grunt.log.writeln(selectors[0]['cyan'].bold);
          firstSelector = selectors[0];
        }
        expandObj(cssObj, selectors, declarations);
      });
    });

    grunt.file.write('.tmp/scripts/themes.json', JSON.stringify(cssObj, null, '  '));
    grunt.log.write('File ');
    grunt.log.write('.tmp/scripts/themes.json'['cyan']);
    grunt.log.writeln(' created');
    grunt.task.run(['json']);
  });

};
