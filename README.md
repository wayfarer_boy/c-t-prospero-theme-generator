## Prospero theme generator

A web page and grunt script that generates the theme.js file required for use by the Prospero extension.

## Usage

* Install the dependencies with ```npm install```
* Run the server with ``` grunt serve ```
* Any changes to main.scss will trigger the generator and a new theme.js file will be created in app/scripts
* To declare a new theme, edit the themes array at the top of Gruntfile.js
* At the moment, there is only one type of element created by Prospero - the overlay - but in the future, we'll look to create many more.

## License

MIT License
Copyright (c) C&T (info@candt.org)
