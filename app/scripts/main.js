'use strict';

var jQuery = window.jQuery || {};
var Handlebars = window.Handlebars || {};

(function ($, Handlebars) {

  var themes = ['prospero', 'unstyled'];
  var data = {
    host: 'http://localhost:9000',
    text: 'Et, posuere non, mollis sit amet.',
    close: true,
    name: 'Sed Mauris',
    avatar: 'test.jpg',
    video: 'test'
  };
  var sizes = ['large', 'small'];
  var suite = [
    {'narrative_box': ['text']},
    {'narrative_box': ['text','close']},
    {'speech_bubble': ['avatar','name','text']},
    {'speech_bubble': ['avatar','name','close','text']},
    {'thought_bubble': ['avatar','text']},
    {'thought_bubble': ['avatar','close','text']},
    {'first_person': ['avatar','name','video']},
    {'first_person': ['avatar','name','video','text']},
    {'first_person': ['avatar','name','close','video','text']},
    {'third_person': ['video']},
    {'third_person': ['video','text']},
    {'third_person': ['close','video','text']},
  ];

  $(document).ready(function () {
    var sections = $('section.themes').width(themes.length * 1000);
    $.each(themes, function (k,theme) {
      var section = $('<section>').addClass('theme '+theme).appendTo(sections);
      section.append('<header><h2>'+theme+': Overlays</h2></header>');
      for (var index in suite) {
        $.each(suite[index], function (preset,keys) {
          for (var size in sizes) {
            section.append('<h4>'+preset+': '+keys.join(', ')+'</h4>');
            var d = {};
            for (var i in keys) {
              d[keys[i]] = data[keys[i]];
            }
            d.size = sizes[size]
            section.append(Handlebars.partials[preset](d));
          }
        });
      }
    });
  });
})(jQuery, Handlebars);
