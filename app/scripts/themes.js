var p = p || {};
p["themes"] = {
  "prospero": {
    "overlay": {
      "base": {
        "background": "#3bc1f5",
        "box-shadow": "0 0 1em 0 rgba(0, 0, 0, 0.8)",
        "display": "table",
        "font": "normal normal normal 16px/1.3 RobotoLight, \"Helvetica Neue\", Arial, Helvetica, sans-serif",
        "position": "relative"
      },
      "others": {
        ".content": {
          "max-width": "640px"
        },
        ".content-small": {
          "max-width": "320px"
        },
        ".sidebar": {
          "background": "#3bc1f5",
          "display": "table-cell",
          "height": "100%",
          "text-align": "center",
          "vertical-align": "top",
          "width": "160px"
        },
        ".sidebar-small": {
          "width": "80px"
        },
        ".content-bar": {
          "display": "table-cell",
          "height": "100%",
          "max-width": "640px",
          "vertical-align": "middle"
        },
        ".content-bar.text-only": {
          "background": "#000"
        },
        ".content-bar-small": {
          "max-width": "320px"
        },
        ".para": {
          "background": "#000",
          "color": "#fff",
          "display": "block",
          "height": "auto",
          "margin": "0",
          "padding": "0 2em 2em"
        },
        ".para:nth-of-type(1)": {
          "padding": "2em"
        },
        ".para.video-text": {
          "margin-top": "-4.5em",
          "padding-top": "4em"
        },
        ".para-small": {
          "font-size": "85%",
          "padding": "0 1.7em 1.7em"
        },
        ".para-small:nth-of-type(1)": {
          "padding": "1.7em"
        },
        ".para-small.video-text": {
          "margin-top": "-2.4em",
          "padding-top": "2em"
        },
        ".media": {
          "box-sizing": "border-box",
          "max-height": "550px",
          "max-width": "640px",
          "padding": "2em",
          "text-align": "center"
        },
        ".media-small": {
          "max-height": "275px",
          "max-width": "320px",
          "padding": "1em"
        },
        ".close": {
          "color": "#fff",
          "cursor": "pointer",
          "font-size": "24px",
          "font-weight": "bold",
          "padding": "0.2em 0.4em",
          "position": "absolute",
          "right": "0",
          "top": "0"
        },
        ".close-small": {
          "font-size": "16px",
          "padding": "0.1em 0.2em",
          "right": "0",
          "top": "0"
        },
        ".icon": {
          "height": "100px",
          "margin-bottom": "-1.2em",
          "margin-top": "2em",
          "width": "100px"
        },
        ".icon-small": {
          "height": "50px",
          "margin-bottom": "-.6em",
          "margin-top": "1em",
          "width": "50px"
        },
        ".avatar": {
          "border": "solid #fff 0.5em",
          "border-radius": "50px",
          "height": "75px",
          "margin-top": "2em",
          "width": "75px"
        },
        ".avatar-small": {
          "border-radius": "25px",
          "border-width": ".25em",
          "height": "40px",
          "margin-top": "1em",
          "width": "40px"
        },
        ".subtitle": {
          "color": "#fff",
          "font-size": "17.6px",
          "font-weight": "bold",
          "line-height": "1.1",
          "margin": "0.5em",
          "margin-bottom": "2em"
        },
        ".subtitle-small": {
          "font-size": "75%",
          "margin": "0.25em",
          "margin-bottom": "1em"
        }
      }
    }
  }
};